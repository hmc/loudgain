Source: loudgain
Section: sound
Priority: optional
Maintainer: Hugh McMaster <hmc@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 cmake,
 libavcodec-dev,
 libavformat-dev,
 libavutil-dev,
 libebur128-dev,
 libswresample-dev,
 libtag1-dev,
 pkgconf,
 zlib1g-dev | libz-dev
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://github.com/Moonbase59/loudgain
Vcs-Git: https://salsa.debian.org/hmc/loudgain.git
Vcs-Browser: https://salsa.debian.org/hmc/loudgain

Package: loudgain
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: ReplayGain 2.0 loudness normalizer based on the EBU R128 standard
 loudgain is a loudness normalizer that scans music files and calculates
 loudness-normalized gain and loudness peak values according to the
 EBU R128/ITU BS.1770 standard (-18 LUFS).
 .
 loudgain will embed ReplayGain 2.0-compatible metadata tags in a file if
 requested but will not modify the audio data in any way.
 .
 loudgain supports the FLAC, Ogg, MP2, MP3, MP4, M4A, ALAC, Opus, ASF, WMA, WAV,
 WavPack, AIFF and APE audio formats. Video files with compatible audio streams
 are also supported.
